package model;

/**
 * Created by oliver on 20/10/16.
 */
public abstract class Problem {
    public abstract String getAnswer();
    public abstract String getQuestion();
}
