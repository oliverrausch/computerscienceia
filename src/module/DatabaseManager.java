package module;

import factory.QuadraticProblemFactory;
import model.Problem;
import model.Quadratic;

import java.io.File;
import java.sql.*;
import java.util.ArrayList;

public class DatabaseManager {
    private final String DB_NAME = "database";
    private Connection conn;
    private Statement stat;

    public DatabaseManager() {
        try {
            File file = new File(this.DB_NAME + ".h2.db");

            boolean createTables = !file.exists() && !file.isDirectory();
            conn = DriverManager.getConnection("jdbc:h2:" + this.DB_NAME + ";TRACE_LEVEL_FILE=0");
            stat = conn.createStatement();

            if (createTables) {
                initDatabase();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initDatabase() {
        //Creating the Tables for the Database
        System.out.println("-- STARTING TABLE CREATION --");
        final String CREATE_PROBLEMS_TABLE = "CREATE TABLE "
                + DatabaseContract.Problems.TABLE_NAME + " ("
                + DatabaseContract.Problems.COLUMN_ID + " INT PRIMARY KEY AUTO_INCREMENT,"
                + DatabaseContract.Problems.COLUMN_PROBLEM_TYPE + " INT,"
                + DatabaseContract.Problems.COLUMN_QUESTION + " TEXT, "
                + DatabaseContract.Problems.COLUMN_DATA + " TEXT,"
                + DatabaseContract.Problems.COLUMN_ANSWER + " TEXT)";


        try {
            stat.execute(CREATE_PROBLEMS_TABLE);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < 30; i++) {
            insertProblem(QuadraticProblemFactory.generateFactoriseQuadraticQuestion(10));
        }
    }

    public Problem[] getProblemsArray() {
        try {
            ResultSet rs = stat.executeQuery("SELECT * FROM " + DatabaseContract.Problems.TABLE_NAME);
            ArrayList<Problem> problems = new ArrayList<>();
            while (rs.next()) {
                if (rs.getInt(DatabaseContract.Problems.COLUMN_PROBLEM_TYPE) == DatabaseContract.Problems.TYPE_QUADRATIC) {
                    problems.add(new Quadratic(rs.getString(DatabaseContract.Problems.COLUMN_DATA),
                            rs.getString(DatabaseContract.Problems.COLUMN_ANSWER),
                            rs.getString(DatabaseContract.Problems.COLUMN_QUESTION)
                    ));
                }
            }
            Problem[] problemsArray = new Problem[problems.size()];
            for (int i = 0; i < problemsArray.length; i++) {
                problemsArray[i] = problems.get(i);
            }
            return problemsArray;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void insertProblem(Problem problem) {
        StringBuilder statement = new StringBuilder("INSERT INTO ")
                .append(DatabaseContract.Problems.TABLE_NAME)
                .append("(")
                .append(DatabaseContract.Problems.COLUMN_PROBLEM_TYPE).append(", ")
                .append(DatabaseContract.Problems.COLUMN_QUESTION).append(", ")
                .append(DatabaseContract.Problems.COLUMN_DATA).append(", ")
                .append(DatabaseContract.Problems.COLUMN_ANSWER).append(")")
                .append(" VALUES(");
        if (problem instanceof Quadratic) {
            Quadratic quadratic = (Quadratic) problem;
            statement.append(String.valueOf(DatabaseContract.Problems.TYPE_QUADRATIC)).append(", ");
            statement.append("\'").append(quadratic.getQuestion()).append("\', ");
            statement.append("\'").append(quadratic.toDatabaseForm()).append("\', ");
            statement.append("\'").append(quadratic.getAnswer()).append("\')");
        }
        System.out.println(statement.toString());
        try {
            stat.execute(statement.toString());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void close() {
        try {
            stat.close();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}


