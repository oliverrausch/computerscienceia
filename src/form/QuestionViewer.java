package form;

import factory.RightAngleTrigonometricProblemFactory;
import form.question.ProblemForm;
import form.question.QuadraticForm;
import form.question.RightAngleTrigonometricForm;
import jiconfont.icons.GoogleMaterialDesignIcons;
import jiconfont.swing.IconFontSwing;
import model.Problem;
import model.Quadratic;
import module.DatabaseManager;

import javax.swing.*;
import java.awt.*;

public class QuestionViewer {
    private static Problem problem;
    private static JFrame frame;
    private static JPanel scrollerPanel, questionPanel;
    private static JList<Problem> list;
    private static Problem[] problems;
    private static ProblemForm currentProblem;
    private static JButton backButton;
    private static JButton addButton;


    private static void initUI() {
        try {
            UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            ex.printStackTrace();
        }
        scrollerPanel = new JPanel(new BorderLayout());
        DatabaseManager db = new DatabaseManager();
        problems = db.getProblemsArray();
        db.close();

        list = new JList<>(problems);
        scrollerPanel.add(new JScrollPane(list, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER), BorderLayout.CENTER);
        backButton = new JButton("Back");
        backButton.setIcon(IconFontSwing.buildIcon(GoogleMaterialDesignIcons.ARROW_BACK, 20, new Color(0, 0, 0)));

        scrollerPanel.add(backButton, BorderLayout.SOUTH);
        addButton = new JButton("Add");
        addButton.setIcon(IconFontSwing.buildIcon(GoogleMaterialDesignIcons.ADD, 20, new Color(0, 150, 0)));

        scrollerPanel.add(addButton, BorderLayout.NORTH);
        frame = new JFrame("Question Viewer");
        questionPanel = new JPanel(new BorderLayout());
        if (problem instanceof Quadratic) {
//            currentProblem = new QuadraticForm((Quadratic) problem);
//            questionPanel.add(currentProblem, BorderLayout.NORTH);
//            questionPanel.add(new JButton("Submit"), BorderLayout.SOUTH);
        }
        questionPanel.add(new RightAngleTrigonometricForm(RightAngleTrigonometricProblemFactory.generateRightAngleTrigonometricProblem(2)), BorderLayout.NORTH);
        JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, scrollerPanel, questionPanel);
        frame.add(splitPane);
        frame.setSize(1000, 1000);
        frame.setResizable(true);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    private static void initListeners() {
        backButton.addActionListener(e -> {
            Start.show();
            frame.dispose();
        });
        list.addListSelectionListener(e -> selectProblem(e.getFirstIndex()));
    }

    static void show() {
        initUI();
        initListeners();
    }

    private static void selectProblem(int index) {
        if (problems[index] instanceof Quadratic) {
            questionPanel.removeAll();
            currentProblem = new QuadraticForm((Quadratic) problems[index]);
            questionPanel.add(currentProblem, BorderLayout.NORTH);
            questionPanel.add(new JButton("Submit"), BorderLayout.SOUTH);
        }
        questionPanel.revalidate();
        frame.revalidate();
    }
}
