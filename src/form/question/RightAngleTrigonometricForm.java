package form.question;

import model.Problem;
import model.RightAngleTrigonometric;

import javax.swing.*;
import java.awt.*;

public class RightAngleTrigonometricForm extends ProblemForm {

    private RightAngleTrigonometric question;
    private JLabel imageLabel;

    public RightAngleTrigonometricForm(Problem problem) {
        super(problem);
        setLayout(new BorderLayout());
        question = (RightAngleTrigonometric) problem;
        imageLabel = new JLabel();
        imageLabel.setIcon(new ImageIcon(question.getImage(), BorderLayout.NORTH));
        add(imageLabel);
    }

    @Override
    boolean checkAnswer() {
        return false;
    }


}
