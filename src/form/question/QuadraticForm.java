package form.question;

import model.Quadratic;
import module.Util;

import javax.swing.*;
import java.awt.*;

public class QuadraticForm extends ProblemForm {

    private Quadratic quadratic;
    private JLabel questionText;
    private JTextField answerField;

    public QuadraticForm(Quadratic quadratic) {
        super(quadratic);
        this.quadratic = quadratic;
        setLayout(new GridLayout(0, 1));

        questionText = new JLabel(Util.superscript(quadratic.getQuestion()));
        add(questionText);

        answerField = new JTextField();
        add(answerField);
    }

    @Override
    boolean checkAnswer() {
        return true;
    }
}
